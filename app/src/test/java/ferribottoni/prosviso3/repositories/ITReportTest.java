package ferribottoni.prosviso3.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.util.Date;

import ferribottoni.prosviso3.models.*;
import ferribottoni.prosviso3.utils.*;
import ferribottoni.prosviso3.utils.Enums.ReportType;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/applicationContext.xml")
@Transactional
public class ITReportTest {

    @Autowired
    public UserRepository userRepository;

    @Autowired
    public PostRepository postRepository;

    @Autowired
    public ReportRepository reportRepository;

    @Autowired
    public TopicRepository topicRepository;

    /** Flush a new Report into database */
    private Report InsertRandomReport() {
        Report report = RandomGenerator.report();
        userRepository.saveAndFlush(report.getReporter());
        userRepository.saveAndFlush(report.getPost().getAuthor());
        postRepository.saveAndFlush(report.getPost());
        return reportRepository.saveAndFlush(report);
    }

    @Test
    public void DependencyInjection() {
        assertNotNull(userRepository);
        assertNotNull(postRepository);
        assertNotNull(reportRepository);
    }

    @Test
    @Transactional
    public void CreateReports() {
        Report report1 = InsertRandomReport();
        Post post2 = topicRepository.saveAndFlush(new Topic("title2", "text2", report1.getPost().getAuthor()));
        Report report2 = reportRepository
                .saveAndFlush(new Report(post2, report1.getReporter(), ReportType.FRAUD, "report2"));

        assertEquals(2, reportRepository.findByReporterId(report1.getReporter().getId()).size());
        assertEquals(1, reportRepository.findByPostId(report2.getPost().getId()).size());
    }
    
    @Test(expected = org.springframework.orm.jpa.JpaSystemException.class)
    public void TextNullFail() {
        Report report = InsertRandomReport();
        report.setText(null);
        reportRepository.saveAndFlush(report);
    }

    @Test(expected = org.springframework.orm.jpa.JpaSystemException.class)
    public void TextTooLongFail() {
        int length;
        try {
            length = Report.class.getDeclaredField("text").getAnnotation(javax.persistence.Column.class).length() + 1;
        } catch (Exception e) {
            length = Integer.MAX_VALUE;
        }
        Report report = InsertRandomReport();
        report.setText(RandomGenerator.stringAlphabetic(length));
        reportRepository.saveAndFlush(report);
    }

    @Test(expected = org.springframework.orm.jpa.JpaSystemException.class)
    public void ReportTypeNullFail() {
        Report report = InsertRandomReport();
        report.setType(null);
        reportRepository.saveAndFlush(report);
    }

    @Test(expected = org.springframework.orm.jpa.JpaSystemException.class)
    public void ReporterNullFail() {
        Report report = InsertRandomReport();
        report.setReporter(null);
        reportRepository.saveAndFlush(report);
    }

    @Test(expected = org.springframework.orm.jpa.JpaSystemException.class)
    public void PostNullFail() {
        Report report = InsertRandomReport();
        report.setPost(null);
        reportRepository.saveAndFlush(report);
    }
    
    @Test
    public void Auditing() {
        Report report = InsertRandomReport();
        report = reportRepository.getOne(report.getId());
        assertNotNull(report);
        assertTrue(report.getCreatedOn().isAfter(LocalDateTime.now().minusSeconds(1)));
        assertTrue(report.getUpdatedOn().isAfter(LocalDateTime.now().minusSeconds(1)));
        assertTrue(report.getCreatedOn().isBefore(LocalDateTime.now().plusSeconds(1)));
        assertTrue(report.getUpdatedOn().isBefore(LocalDateTime.now().plusSeconds(1)));
    }

    @Test
    public void SearchReport() {
        Report report = InsertRandomReport();
        report.setText("This user is spamming.");
        assertTrue(reportRepository.findByPostId(report.getPost().getId()).contains(report));
        assertTrue(reportRepository.findByReporterId(report.getReporter().getId()).contains(report));
        assertTrue(reportRepository.findByTextIgnoreCaseContaining("spam").contains(report));
        assertTrue(reportRepository.findByCreatedOnBetween(report.getCreatedOn().minusHours(1), report.getCreatedOn().plusHours(1)).contains(report));
    }

    @Test
    @Transactional(propagation = Propagation.NEVER)
    public void CascadingDeletePost() {
        Report report = InsertRandomReport();
        assertTrue(reportRepository.exists(report.getId()));
        postRepository.delete(report.getPost().getId()); // delete post
        assertFalse(reportRepository.exists(report.getId())); // check
        userRepository.delete(report.getPost().getAuthor().getId()); // delete post author
        userRepository.delete(report.getReporter().getId()); // delete reporter
    }

    @Test
    @Transactional(propagation = Propagation.NEVER)
    public void CascadingDeleteReporter() {
        Report report = InsertRandomReport();
        assertTrue(reportRepository.exists(report.getId()));
        userRepository.delete(report.getReporter().getId()); // delete reporter
        assertFalse(reportRepository.exists(report.getId())); // check
        postRepository.delete(report.getPost().getId()); // delete post
        userRepository.delete(report.getPost().getAuthor().getId()); // delete post author
    }

    @Test
    @Transactional(propagation = Propagation.NEVER)
    public void CascadingDeletePostAuthor() {
        Report report = InsertRandomReport();
        assertTrue(reportRepository.exists(report.getId()));
        userRepository.delete(report.getPost().getAuthor().getId()); // delete post author
        assertFalse(reportRepository.exists(report.getId())); // check
        userRepository.delete(report.getReporter().getId()); // delete reporter
    }
}