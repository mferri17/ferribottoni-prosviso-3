package ferribottoni.prosviso3.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import ferribottoni.prosviso3.models.*;
import ferribottoni.prosviso3.models.Topic;
import ferribottoni.prosviso3.utils.*;
import ferribottoni.prosviso3.utils.Enums.ReportType;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/applicationContext.xml")
@Transactional
public class ITTopicTest {

    @Autowired
    public UserRepository userRepository;

    @Autowired
    public TopicRepository topicRepository;

    @Autowired
    public ReportRepository reportRepository;

    @Autowired
    public CommentRepository commentRepository;

    /** Flush a new Topic into database */
    private Topic InsertRandomTopic() {
        Topic topic = RandomGenerator.topic();
        userRepository.saveAndFlush(topic.getAuthor());
        return topicRepository.saveAndFlush(topic);
    }

    @Test
    public void DependencyInjection() {
        assertNotNull(userRepository);
        assertNotNull(topicRepository);
    }

    @Test
    @Transactional
    public void CreateTopic() {
        Topic newTopic = InsertRandomTopic();

        // search and check
        Topic found = topicRepository.findOne(newTopic.getId());
        assertNotNull(found);
        assertEquals(found.getTitle(), newTopic.getTitle());
        assertEquals(found.getText(), newTopic.getText());
        assertEquals(found.getAuthor(), newTopic.getAuthor());
        assertEquals(found.getViews(), newTopic.getViews());
        assertEquals(found.getCreatedOn(), newTopic.getCreatedOn());
        assertEquals(found.getUpdatedOn(), newTopic.getUpdatedOn());
    }

    @Test
    public void CreateAndUpdateTopic() {
        // create and check
        Topic newTopic = InsertRandomTopic();
        Topic found1 = topicRepository.findOne(newTopic.getId());
        assertNotNull(found1);
        assertEquals(found1.getTitle(), newTopic.getTitle());

        // search and update
        String newTitle = RandomGenerator.stringAlphabetic(10);
        found1.setTitle(newTitle);
        topicRepository.saveAndFlush(found1);

        // search and check
        Topic found2 = topicRepository.findOne(newTopic.getId());
        assertNotNull(found2);
        assertEquals(found2.getTitle(), newTitle);
    }

    @Test
    public void CreateAndDeleteTopic() {
        Topic newTopic = InsertRandomTopic();

        // search and delete
        Topic found1 = topicRepository.findOne(newTopic.getId());
        assertNotNull(found1);
        topicRepository.delete(found1);

        // search and check
        assertFalse(topicRepository.exists(newTopic.getId()));
    }

    @Test(expected = org.springframework.orm.jpa.JpaSystemException.class)
    public void TitleNullFail() {
        Topic newTopic = InsertRandomTopic();
        newTopic.setTitle(null);
        topicRepository.saveAndFlush(newTopic);
    }
    

    @Test(expected = org.springframework.orm.jpa.JpaSystemException.class)
    public void TextNullFail() {
        Topic newTopic = InsertRandomTopic();
        newTopic.setText(null);
        topicRepository.saveAndFlush(newTopic);
    }

    @Test(expected = org.springframework.orm.jpa.JpaSystemException.class)
    public void TitleTooLongFail() {
        int length;
        try {
            length = Topic.class.getDeclaredField("title").getAnnotation(javax.persistence.Column.class).length() + 1;
        } catch (Exception e) {
            length = Integer.MAX_VALUE;
        }
        String title = RandomGenerator.stringAlphabetic(length);

        Topic newTopic = RandomGenerator.topic();
        newTopic.setTitle(title);
        userRepository.saveAndFlush(newTopic.getAuthor());
        topicRepository.saveAndFlush(newTopic);
    }

    @Test
    @Transactional(propagation = Propagation.NEVER)
    public void CascadingDeleteAuthor() {
        Topic newTopic = InsertRandomTopic();

        userRepository.delete(newTopic.getAuthor().getId());
        assertFalse(userRepository.exists(newTopic.getAuthor().getId()));
        assertFalse(topicRepository.exists(newTopic.getId()));
    }

    @Test
    public void Auditing() {
        Topic topic = InsertRandomTopic();
        topic = topicRepository.getOne(topic.getId());
        assertNotNull(topic);
        assertTrue(topic.getCreatedOn().isAfter(LocalDateTime.now().minusSeconds(1)));
        assertTrue(topic.getUpdatedOn().isAfter(LocalDateTime.now().minusSeconds(1)));
        assertTrue(topic.getCreatedOn().isBefore(LocalDateTime.now().plusSeconds(1)));
        assertTrue(topic.getUpdatedOn().isBefore(LocalDateTime.now().plusSeconds(1)));
    }

    @Test
    public void Search() {
        Topic topic = InsertRandomTopic();
        topic.setTitle("Foodability");
        topic.setText("I really like PIZZA!");
        topicRepository.saveAndFlush(topic);
        assertTrue(topicRepository.findByTitleIgnoreCaseContaining("Food").contains(topic));
        assertTrue(topicRepository.findByTextIgnoreCaseContaining("pizza").contains(topic));
        assertTrue(topicRepository.findByCreatedOnBetween(topic.getCreatedOn().minusHours(1), topic.getCreatedOn().plusHours(1)).contains(topic));
    }

    @Test
    @Transactional
    public void CreateReportsThenDeleteTopic() {
        long initialReportsCount = reportRepository.count();

        // create 2 users
        User user1 = userRepository.saveAndFlush(RandomGenerator.user());
        User user2 = userRepository.saveAndFlush(RandomGenerator.user());

        // create topic and reports
        User author = userRepository.saveAndFlush(RandomGenerator.user());
        Topic topic = topicRepository.saveAndFlush(new Topic("title", "test text", author));
        topic.addReport(new Report(topic, user1, ReportType.OFFENSIVE, "report 1 text"));
        topic.addReport(new Report(topic, user2, ReportType.OFFENSIVE, "report 2 text"));
        topic = topicRepository.saveAndFlush(topic);

        // check
        assertEquals(initialReportsCount + 2, reportRepository.count());
        assertEquals(2, reportRepository.findByPostId(topic.getId()).size());

        // delete topic
        topicRepository.delete(topic);
        assertFalse(topicRepository.exists(topic.getId()));

        // check reports has been deleted
        assertEquals(initialReportsCount, reportRepository.count());
    }

    @Test
    @Transactional
    public void CreateRepliesThenDeleteTopic() {
        long initialCommentsCount = commentRepository.count();

        // create topic
        Topic topic = InsertRandomTopic();

        // create 2 replies
        User responder = userRepository.saveAndFlush(RandomGenerator.user());
        topic.addReply(new Comment("test reply 1", responder, topic));
        topic.addReply(new Comment("test reply 2", responder, topic));
        topic = topicRepository.saveAndFlush(topic);

        // check
        assertEquals(initialCommentsCount + 2, commentRepository.count());
        assertEquals(2, commentRepository.findByAuthorId(responder.getId()).size());

        // delete topic
        topicRepository.delete(topic);
        assertFalse(topicRepository.exists(topic.getId()));

        // check reports has been deleted
        assertEquals(initialCommentsCount, reportRepository.count());
    }

    @Test
    @Transactional
    public void AddLikesThenDeleteTopic() {
        // create users
        User user1 = userRepository.saveAndFlush(RandomGenerator.user());
        User user2 = userRepository.saveAndFlush(RandomGenerator.user());

        // add likes
        Topic newTopic = InsertRandomTopic();
        newTopic.addLike(user1);
        newTopic.addLike(user2);
        newTopic = topicRepository.saveAndFlush(newTopic);

        // check likes
        assertEquals(newTopic.getLikes().size(), topicRepository.findOne(newTopic.getId()).getLikes().size());

        // delete and check likes
        newTopic.getLikes().remove(user2);
        newTopic = topicRepository.saveAndFlush(newTopic);
        assertEquals(newTopic.getLikes().size(), topicRepository.findOne(newTopic.getId()).getLikes().size());

        // delete topic with no errors
        topicRepository.delete(newTopic);
        assertNull(topicRepository.findOne(newTopic.getId()));
    }
}