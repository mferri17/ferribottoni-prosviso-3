package ferribottoni.prosviso3.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import ferribottoni.prosviso3.models.*;
import ferribottoni.prosviso3.utils.*;
import ferribottoni.prosviso3.utils.Enums.ReportType;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/applicationContext.xml")
@Transactional
public class ITCommentTest {

    @Autowired
    public UserRepository userRepository;

    @Autowired
    public PostRepository postRepository;

    @Autowired
    public CommentRepository commentRepository;

    @Autowired
    public ReportRepository reportRepository;

    /** Flush a new Comment into database */
    private Comment InsertRandomComment() {
        Comment newComment = RandomGenerator.comment();
        userRepository.saveAndFlush(newComment.getAuthor());
        userRepository.saveAndFlush(newComment.getReplyTo().getAuthor());
        postRepository.saveAndFlush(newComment.getReplyTo());
        return commentRepository.saveAndFlush(newComment);
    }

    @Test
    public void DependencyInjection() {
        assertNotNull(userRepository);
        assertNotNull(commentRepository);
    }

    @Test
    @Transactional
    public void CreateComment() {
        Comment newComment = InsertRandomComment();
        Comment found = commentRepository.findOne(newComment.getId());
        assertNotNull(found);
        assertEquals(newComment.getText(), found.getText());
        assertEquals(newComment.getCreatedOn(), found.getCreatedOn());
        assertEquals(newComment.getUpdatedOn(), found.getUpdatedOn());
        assertEquals(newComment.getAuthor(), found.getAuthor());
        assertEquals(1, commentRepository.findByReplyToId(found.getReplyTo().getId()).size());
    }

    @Test
    public void CreateAndUpdateComment() {
        // create and check
        Comment newComment = InsertRandomComment();
        Comment found1 = commentRepository.findOne(newComment.getId());
        assertNotNull(found1);
        assertEquals(newComment.getText(), found1.getText());

        // search and update
        String newText = RandomGenerator.stringAlphabetic(10);
        found1.setText(newText);
        commentRepository.saveAndFlush(found1);

        // search and check
        Comment found2 = commentRepository.findOne(newComment.getId());
        assertNotNull(found2);
        assertEquals(newText, found2.getText());
    }

    @Test
    public void CreateAndDeleteComment() {
        Comment newComment = InsertRandomComment();

        // search and delete
        Comment found1 = commentRepository.findOne(newComment.getId());
        assertNotNull(found1);
        commentRepository.delete(found1);
        commentRepository.flush();

        // search and check
        assertFalse(commentRepository.exists(newComment.getId()));
    }

    @Test(expected = org.springframework.orm.jpa.JpaSystemException.class)
    public void TextNullFail() {
        Comment comment = InsertRandomComment();
        comment.setText(null);
        commentRepository.saveAndFlush(comment);
    }

    @Test(expected = org.springframework.orm.jpa.JpaSystemException.class)
    public void ReplyToNullFail() {
        Comment comment  = InsertRandomComment();
        comment.setReplyTo(null);
        commentRepository.saveAndFlush(comment);
    }

    @Test
    @Transactional(propagation = Propagation.NEVER)
    public void CascadingDeleteAuthor() {
        Comment newComment = InsertRandomComment();

        userRepository.delete(newComment.getAuthor().getId());
        assertFalse(userRepository.exists(newComment.getAuthor().getId()));
        assertFalse(commentRepository.exists(newComment.getId()));

        // garbage collection
        postRepository.delete(newComment.getReplyTo());
        userRepository.delete(newComment.getReplyTo().getAuthor());
        userRepository.delete(newComment.getAuthor());
    }

    @Test
    @Transactional(propagation = Propagation.NEVER)
    public void CascadingDeleteReplyTo() {
        Comment newComment = InsertRandomComment();

        postRepository.delete(newComment.getReplyTo().getId());
        assertFalse(postRepository.exists(newComment.getReplyTo().getId()));
        assertFalse(commentRepository.exists(newComment.getId()));

        // garbage collection
        userRepository.delete(newComment.getReplyTo().getAuthor()); 
        userRepository.delete(newComment.getAuthor()); 
    }

    @Test
    public void Auditing() {
        Comment comment = InsertRandomComment();
        comment = commentRepository.getOne(comment.getId());
        assertNotNull(comment);
        assertTrue(comment.getCreatedOn().isAfter(LocalDateTime.now().minusSeconds(1)));
        assertTrue(comment.getUpdatedOn().isAfter(LocalDateTime.now().minusSeconds(1)));
        assertTrue(comment.getCreatedOn().isBefore(LocalDateTime.now().plusSeconds(1)));
        assertTrue(comment.getUpdatedOn().isBefore(LocalDateTime.now().plusSeconds(1)));
    }

    @Test
    public void Search() {
        Comment comment = InsertRandomComment();
        comment.setText("I really like PIZZA!");
        commentRepository.saveAndFlush(comment);
        assertTrue(commentRepository.findByTextIgnoreCaseContaining("pizza").contains(comment));
        assertTrue(commentRepository.findByReplyToId(comment.getReplyTo().getId()).contains(comment));
        assertTrue(commentRepository.findByCreatedOnBetween(comment.getCreatedOn().minusHours(1), comment.getCreatedOn().plusHours(1)).contains(comment));
    }

    @Test
    @Transactional
    public void CreateReportsThenDeleteComment() {
        long initialReportsCount = reportRepository.count();

        // create comment
        Comment comment = InsertRandomComment();

        // create reports
        User reporter = userRepository.saveAndFlush(RandomGenerator.user());
        comment.addReport(new Report(comment, reporter, ReportType.SPAM, "report 1 text"));
        comment.addReport(new Report(comment, reporter, ReportType.OFFENSIVE, "report 1 text"));
        commentRepository.saveAndFlush(comment);

        // check
        assertEquals(initialReportsCount + 2, reportRepository.count());
        assertEquals(2, reportRepository.findByPostId(comment.getId()).size());

        // delete topic
        commentRepository.delete(comment);
        assertFalse(commentRepository.exists(comment.getId()));

        // check reports has been deleted
        assertEquals(initialReportsCount, reportRepository.count());
    }

    @Test
    @Transactional
    public void CreateRepliesThenDeleteComment() {
        long initialCommentsCount = commentRepository.count();

        // create topic
        Comment comment = InsertRandomComment();

        // create 2 replies
        User responder = userRepository.saveAndFlush(RandomGenerator.user());
        comment.addReply(new Comment("test reply 1", responder, comment));
        comment.addReply(new Comment("test reply 2", responder, comment));
        comment = commentRepository.saveAndFlush(comment);

        // check
        assertEquals(initialCommentsCount + 3, commentRepository.count());
        assertEquals(2, commentRepository.findByReplyToId(comment.getId()).size());

        // delete topic
        commentRepository.delete(comment);
        assertFalse(commentRepository.exists(comment.getId()));

        // check reports has been deleted
        assertEquals(initialCommentsCount, reportRepository.count());
    }

    @Test
    @Transactional
    public void AddLikesThenDeleteComment() {
        // create users
        User user1 = userRepository.saveAndFlush(RandomGenerator.user());
        User user2 = userRepository.saveAndFlush(RandomGenerator.user());

        // add likes
        Comment newComment = InsertRandomComment();
        newComment.addLike(user1);
        newComment.addLike(user2);
        newComment = commentRepository.saveAndFlush(newComment);

        // check likes
        assertEquals(newComment.getLikes().size(), commentRepository.findOne(newComment.getId()).getLikes().size());

        // delete and check likes
        newComment.getLikes().remove(user2);
        newComment = commentRepository.saveAndFlush(newComment);
        assertEquals(newComment.getLikes().size(), commentRepository.findOne(newComment.getId()).getLikes().size());

        // delete comment with no errors
        commentRepository.delete(newComment);
        assertNull(commentRepository.findOne(newComment.getId()));
    }
}