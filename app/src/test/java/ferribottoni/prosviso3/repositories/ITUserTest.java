package ferribottoni.prosviso3.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

import ferribottoni.prosviso3.models.*;
import ferribottoni.prosviso3.models.Topic;
import ferribottoni.prosviso3.utils.*;
import ferribottoni.prosviso3.utils.Enums.ReportType;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/applicationContext.xml")
@Transactional
public class ITUserTest {

    @Autowired
    public UserRepository userRepository;

    @Autowired
    public TopicRepository topicRepository;

    @Autowired
    public ReportRepository reportRepository;

    @Test
    public void DependencyInjection() {
        assertNotNull(userRepository);
        assertNotNull(topicRepository);
    }

    @Test
    public void CreateUser() {
        // create
        User newUser = userRepository.saveAndFlush(RandomGenerator.user());

        // search and check
        User found = userRepository.findByUsername(newUser.getUsername());
        assertNotNull(found);
        assertEquals(newUser.getUsername(), found.getUsername());
        assertEquals(newUser.getPassword(), found.getPassword());
        assertEquals(newUser.getName(), found.getName());
        assertEquals(newUser.getSurname(), found.getSurname());
    }

    @Test
    public void CreateAndUpdateUser() {
        // create and check
        User newUser = userRepository.saveAndFlush(RandomGenerator.user());
        User found1 = userRepository.findByUsername(newUser.getUsername());
        assertNotNull(found1);
        assertEquals(newUser.getName(), found1.getName());

        // search and update
        String newName = RandomGenerator.stringAlphabetic(10);
        found1.setName(newName);
        userRepository.saveAndFlush(found1);

        // search and check
        User found2 = userRepository.findByUsername(found1.getUsername());
        assertNotNull(found2);
        assertEquals(newName, found2.getName());
    }

    @Test
    public void CreateAndDeleteUser() {
        // create
        User newUser = userRepository.saveAndFlush(RandomGenerator.user());

        // search and delete
        User found1 = userRepository.findByUsername(newUser.getUsername());
        assertNotNull(found1);
        userRepository.delete(found1);
        userRepository.flush();

        // search and check
        assertNull(userRepository.findByUsername(newUser.getUsername()));
    }

    @Test
    public void IdAutoIncrement() {
        User newUser1 = userRepository.saveAndFlush(RandomGenerator.user());
        User newUser2 = userRepository.saveAndFlush(RandomGenerator.user());
        assertTrue(newUser1.getId() + 1 == newUser2.getId());
    }

    @Test(expected = org.springframework.orm.jpa.JpaSystemException.class)
    public void UsernameNullFail() {
        User newUser = new User(null, "pwd");
        userRepository.saveAndFlush(newUser);
    }

    @Test(expected = org.springframework.orm.jpa.JpaSystemException.class)
    public void UsernameTooLongFail() {
        int length;
        try {
            length = User.class.getDeclaredField("username").getAnnotation(javax.persistence.Column.class).length() + 1;
        } catch (Exception e) {
            length = Integer.MAX_VALUE;
        }
        String username = RandomGenerator.stringAlphabetic(length);
        User newUser = new User(username, "pwd");
        userRepository.saveAndFlush(newUser);
    }

    @Test(expected = org.springframework.orm.jpa.JpaSystemException.class)
    public void UsernameDuplicatedFail() {
        String username = RandomGenerator.stringAlphabetic(10);
        User newUser1 = new User(username, "pwd1");
        userRepository.saveAndFlush(newUser1);
        User newUser2 = new User(username, "pwd2");
        userRepository.saveAndFlush(newUser2);
    }

    @Test
    public void SearchUser() {
        User newUser = userRepository.saveAndFlush(RandomGenerator.user());
        assertTrue(userRepository.findByUsername(newUser.getUsername()).equals(newUser));
        assertTrue(userRepository.findByUsernameAndPassword(newUser.getUsername(), newUser.getPassword()).equals(newUser));
        assertTrue(userRepository.findByNameIgnoreCase(newUser.getName()).contains(newUser));
        assertTrue(userRepository.findBySurnameIgnoreCase(newUser.getSurname()).contains(newUser));
    }

    @Test
    @Transactional
    public void CreatePostsThenDeleteUser() {
        long initialPostsCount = topicRepository.count();

        // create user
        User user = RandomGenerator.user();

        // add 2 user posts
        user.addPost(new Topic("title 1", "test text 1", user));
        user.addPost(new Topic("title 2", "test text 2", user));
        user = userRepository.saveAndFlush(user);

        // check posts has been created
        assertEquals(user.getPosts().size(), topicRepository.findByAuthorId(user.getId()).size());
        assertEquals(initialPostsCount + 2, topicRepository.count());

        // delete user
        userRepository.delete(user);
        assertFalse(userRepository.exists(user.getId()));

        // check posts has been deleted
        assertEquals(initialPostsCount, topicRepository.count());
    }

    @Test
    @Transactional
    public void CreateReportsThenDeleteUser() {
        long initialReportsCount = reportRepository.count();

        // create 2 posts
        User author = RandomGenerator.user();
        author.addPost(new Topic("title 1", "test text 1", author));
        author.addPost(new Topic("title 2", "test text 2", author));
        author = userRepository.saveAndFlush(author);
        assertEquals(author.getPosts().size(), topicRepository.findByAuthorId(author.getId()).size());

        // create user and reports
        User user = RandomGenerator.user();
        for (Post post : author.getPosts()) {
            user.addReport(new Report(post, user, ReportType.OFFENSIVE, "report text"));
        }
        user = userRepository.saveAndFlush(user);

        // check
        assertEquals(initialReportsCount + author.getPosts().size(), reportRepository.count());
        assertEquals(user.getReports().size(), reportRepository.findByReporterId(user.getId()).size());
        for (Post post : author.getPosts()) {
            assertEquals(1, reportRepository.findByPostId(post.getId()).size());
        }

        // delete user
        userRepository.delete(user);
        assertFalse(userRepository.exists(user.getId()));

        // check reports has been deleted
        assertEquals(initialReportsCount, reportRepository.count());
    }

    @Test
    @Transactional
    public void AddLikesThenDeleteUser() {
        // create posts
        User u = userRepository.save(RandomGenerator.user());
        Post post1 = topicRepository.save(new Topic("title 1", "test text 1", u));
        Post post2 = topicRepository.save(new Topic("title 2", "test text 2", u));

        // create user and add likes
        User user = userRepository.save(RandomGenerator.user());
        user.addLike(post1);
        user.addLike(post2);
        user = userRepository.saveAndFlush(user);

        // check likes
        assertEquals(user.getLikes().size(), userRepository.findOne(user.getId()).getLikes().size());

        // delete and check likes
        user.getLikes().remove(post1);
        user = userRepository.saveAndFlush(user);
        assertEquals(user.getLikes().size(), userRepository.findOne(user.getId()).getLikes().size());

        // delete user with no errors
        userRepository.delete(user);
        assertFalse(userRepository.exists(user.getId()));
    }
}