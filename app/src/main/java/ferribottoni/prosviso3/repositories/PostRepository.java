package ferribottoni.prosviso3.repositories;

import java.util.List;
import java.time.LocalDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ferribottoni.prosviso3.models.Post;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    List<Post> findByCreatedOnBetween(LocalDateTime createdOnStart, LocalDateTime createdOnEnd);
    List<Post> findByAuthorId(Long authorId);
    List<Post> findByTextIgnoreCaseContaining(String text);
    
}