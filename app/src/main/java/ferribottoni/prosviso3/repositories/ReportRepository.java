package ferribottoni.prosviso3.repositories;

import java.util.List;
import java.time.LocalDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ferribottoni.prosviso3.models.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {

    List<Report> findByPostId(Long postId);
    List<Report> findByReporterId(Long authorId);
    List<Report> findByTextIgnoreCaseContaining(String text);
    List<Report> findByCreatedOnBetween(LocalDateTime createdOnStart, LocalDateTime createdOnEnd);

}