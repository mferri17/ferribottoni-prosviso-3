package ferribottoni.prosviso3.repositories;

import java.util.List;
import java.time.LocalDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ferribottoni.prosviso3.models.Topic;

@Repository
public interface TopicRepository extends JpaRepository<Topic, Long> {

    List<Topic> findAllByOrderByUpdatedOnDesc();
    List<Topic> findByCreatedOnBetween(LocalDateTime createdOnStart, LocalDateTime createdOnEnd);
    List<Topic> findByAuthorId(Long authorId);
    List<Topic> findByTextIgnoreCaseContaining(String text);

    List<Topic> findByTitleIgnoreCaseContaining(String title);
    List<Topic> findByisOpen(Boolean isOpen);
    
}