package ferribottoni.prosviso3.repositories;

import java.util.List;
import java.time.LocalDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ferribottoni.prosviso3.models.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findByCreatedOnBetween(LocalDateTime createdOnStart, LocalDateTime createdOnEnd);
    List<Comment> findByAuthorId(Long authorId);
    List<Comment> findByTextIgnoreCaseContaining(String text);
    
    List<Comment> findByReplyToId(Long replyToId);

}