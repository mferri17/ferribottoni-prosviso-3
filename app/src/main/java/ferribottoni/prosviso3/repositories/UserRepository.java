package ferribottoni.prosviso3.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ferribottoni.prosviso3.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
    User findByUsernameAndPassword(String username, String password);
    List<User> findByNameIgnoreCase(String name);
    List<User> findBySurnameIgnoreCase(String surname);

}