package ferribottoni.prosviso3.models;

import javax.persistence.*;

import java.util.Set;
import java.util.HashSet;

@Entity
@Table(name = "users")
public class User implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    // --------------- Attributes

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, length = 64, unique = true)
    private String username;

    @Column(nullable = false, length = 128)
    private String password;

    @Column(nullable = true, length = 64)
    private String name;

    @Column(nullable = true, length = 64)
    private String surname;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "author")
    private Set<Post> posts = new HashSet<Post>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "reporter")
    private Set<Report> reports = new HashSet<Report>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "likes", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = {
            @JoinColumn(name = "post_id") })
    private Set<Post> likes = new HashSet<Post>();

    // --------------- Constructors

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String username, String password, String name, String surname) {
        this(username, password);
        this.name = name;
        this.surname = surname;
    }

    // --------------- Getters and Setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Set<Post> getPosts() {
        return this.posts;
    }

    public void setPosts(Set<Post> posts) {
        this.posts = posts;
    }

    public void addPost(Post post) {
        this.posts.add(post);
    }

    public Set<Post> getLikes() {
        return this.likes;
    }

    public void setLikes(Set<Post> likes) {
        this.likes = likes;
    }

    public void addLike(Post post) {
        this.likes.add(post);
    }

    public Set<Report> getReports() {
        return this.reports;
    }

    public void setReports(Set<Report> reports) {
        this.reports = reports;
    }

    public void addReport(Report report) {
        this.reports.add(report);
    }

    // --------------- Other Methods

    public String getFullName() {
        return String.format("%s %s", name, surname);
    }

    @Override
    public String toString() {
        return String.format("User [id=%d, username=%s, name=%s, surname=%s]", id, username, name, surname);
    }
}