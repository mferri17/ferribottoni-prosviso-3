package ferribottoni.prosviso3.models;

import javax.persistence.*;

@Entity
@DiscriminatorValue("Comment")
public class Comment extends Post {
    private static final long serialVersionUID = 1L;

    // ---------- Attributes

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "replyTo_id", nullable = false)
    private Post replyTo;

    // ---------- Constructors

    public Comment() {
        super();
    }

    public Comment(String text, User author, Post replyTo) {
        super(text, author);
        this.replyTo = replyTo;
    }

    // ---------- Getters and Setters

    public Post getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(Post replyTo) {
        this.replyTo = replyTo;
    }

    // ---------- Other Methods
}