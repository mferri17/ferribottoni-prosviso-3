package ferribottoni.prosviso3.models;

import javax.persistence.*;

@Entity
@DiscriminatorValue("Topic")
public class Topic extends Post {
    private static final long serialVersionUID = 1L;

    // ---------- Attributes

    @Column(nullable = false, length = 256)
    private String title;

    @Column(nullable = false, columnDefinition = "int default 1")
    private Integer views;

    @Column(nullable = false, columnDefinition = "boolean default true")
    private Boolean isOpen;

    // ---------- Constructors

    public Topic() {
        super();
        this.views = 0;
        this.isOpen = true;
    }

    public Topic(String title, String text, User author) {
        this(title, text, author, true, 0);
    }

    public Topic(String title, String text, User author, boolean isOpen, int views) {
        super(text, author);
        this.title = title;
        this.isOpen = isOpen;
        this.views = views;
    }

    // ---------- Getters and Setters

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public Boolean getIsOpen() {
        return this.isOpen;
    }

    public void setIsOpen(Boolean isOpen) {
        this.isOpen = isOpen;
    }


    // ---------- Other Methods

    public void toggleIsOpen() {
        this.isOpen = isOpen ? false : true;
    }
}