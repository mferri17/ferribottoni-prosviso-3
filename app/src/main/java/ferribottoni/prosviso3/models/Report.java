package ferribottoni.prosviso3.models;

import javax.persistence.*;

import ferribottoni.prosviso3.utils.Enums.ReportType;

@Entity
public class Report extends Auditable implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    // ---------- Attributes

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private ReportType type;

    @Column(nullable = false, length = 250)
    private String text;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "reporter_id", nullable = false)
    private User reporter;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "post_id", nullable = false)
    private Post post;

    // ---------- Constructors

    public Report() {
    }

    public Report(Post post, User reporter, ReportType type, String text) {
        this.post = post;
        this.reporter = reporter;
        this.type = type;
        this.text = text;
    }

    // ---------- Getters and Setters

    public Long getId() {
        return id;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public User getReporter() {
        return reporter;
    }

    public void setReporter(User reporter) {
        this.reporter = reporter;
    }

    public ReportType getType() {
        return type;
    }

    public void setType(ReportType type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    // ---------- Other Methods

    @Override
    public String toString() {
        return String.format("Report [id=%d, text=%s, createdOn=%s, reporter=%s, post=%s]", id, text,
                getCreatedOn().toString(), reporter, post);
    }
}