package ferribottoni.prosviso3.models;

import javax.persistence.*;

import java.util.Set;
import java.util.HashSet;
import java.util.Arrays;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "Type")
public abstract class Post extends Auditable implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    // ---------- Attributes

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, columnDefinition = "varchar") // no limit string
    private String text;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "author_id", nullable = false)
    private User author;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "post")
    private Set<Report> reports = new HashSet<Report>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "replyTo")
    private Set<Comment> replies = new HashSet<Comment>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "likes")
    private Set<User> likes = new HashSet<User>();

    // ---------- Constructors

    public Post() {
    }

    public Post(String text, User author) {
        this.text = text;
        this.author = author;
    }

    // ---------- Getters and Setters

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Set<User> getLikes() {
        return this.likes;
    }

    public void setLikes(Set<User> likes) {
        this.likes = likes;
    }

    public void addLike(User user) {
        this.likes.add(user);
    }

    public Set<Comment> getReplies() {
        return this.replies;
    }

    public void setReplies(Set<Comment> replies) {
        this.replies = replies;
    }

    public void addReply(Comment comment) {
        this.replies.add(comment);
    }

    public Set<Report> getReports() {
        return this.reports;
    }

    public void setReports(Set<Report> reports) {
        this.reports = reports;
    }

    public void addReport(Report report) {
        this.reports.add(report);
    }

    // ---------- Other Methods

    @Override
    public String toString() {
        return String.format("Post [id=%d, text=%s, author=%s, createdOn=%s, updatedOn=%s, replies=%s]", id, text,
                author, getCreatedOn().toString(), getUpdatedOn().toString(),
                Arrays.toString(getReplies().toArray()));
    }
}