package ferribottoni.prosviso3.utils;

import java.util.UUID;

import org.apache.commons.text.RandomStringGenerator;

import ferribottoni.prosviso3.models.*;
import ferribottoni.prosviso3.utils.Enums.ReportType;

public class RandomGenerator {

    public static String stringAlphabetic(final int length) {
        return new RandomStringGenerator.Builder().withinRange('a', 'z').build().generate(length);
    }

    public static User user() {
        String username = UUID.randomUUID().toString();
        String password = stringAlphabetic(10);
        String name = stringAlphabetic(10);
        String surname = stringAlphabetic(10);
        return new User(username, password, name, surname);
    }

    /**
     * If used with JPA, you must save() the topic.author first, or you will get an exception.
     */
    public static Topic topic() {
        String title = stringAlphabetic(10);
        String text = stringAlphabetic(100);
        User author = user();
        return new Topic(title, text, author, false, 2);
    }

    /**
     * If used with JPA, you must save() comment.author first and comment.repliesTo afterwards, or you will get an exception.
     */
    public static Comment comment() {
        String text = stringAlphabetic(100);
        User author = user();
        Topic replyTo = topic();
        return new Comment(text, author, replyTo);
    }

    /**
     * If used with JPA, you must save() report.post.author first and report.post afterwards, or you will get an exception.
     */
    public static Report report() {
        Topic topic = topic();
        User reporter = user();
        String text = stringAlphabetic(10);
        return new Report(topic, reporter, ReportType.OTHER, text);
    }
}