package ferribottoni.prosviso3.utils;

public class Enums {
    public enum ReportType {
        SPAM, OFFENSIVE, RACISM, FAKENEWS, FRAUD, OTHER
    }
}