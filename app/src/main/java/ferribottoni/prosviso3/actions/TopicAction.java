package ferribottoni.prosviso3.actions;

import java.util.List;
import com.opensymphony.xwork2.ActionSupport;
import org.springframework.beans.factory.annotation.Autowired;

import ferribottoni.prosviso3.models.Topic;
import ferribottoni.prosviso3.models.User;
import ferribottoni.prosviso3.repositories.TopicRepository;
import ferribottoni.prosviso3.repositories.UserRepository;

public class TopicAction extends ActionSupport {
  private static final long serialVersionUID = 1L;

  @Autowired
  private TopicRepository topicRepository;

  @Autowired
  private UserRepository userRepository;

  // ------- PARAMETERS

  private Topic topic;
  private Long id;
  private Long authorId;

  private List<Topic> topics;
  private List<User> usersSelect;

  public List<Topic> getTopics() {
    return topics;
  }

  public void setTopics(List<Topic> topics) {
    this.topics = topics;
  }

  public List<User> getUsersSelect() {
    return usersSelect;
  }

  public void setAuthorId(Long authorId) {
    this.authorId = authorId;
  }

  public Topic getTopic() {
    return topic;
  }

  public void setTopic(Topic topic) {
    this.topic = topic;
  }

  public void setId(Long id) {
    this.id = id;
  }

  // ------- ACTIONS

  public String findAll() throws Exception {
    topics = topicRepository.findAllByOrderByUpdatedOnDesc();
    usersSelect = userRepository.findAll();
    return SUCCESS;
  }

  public String selectTopic() {
    this.topic = topicRepository.findOne(this.id);
    if(this.topic.getIsOpen() == false) return ERROR;
    authorId = this.topic.getAuthor().getId();
    usersSelect = userRepository.findAll();
    topics = topicRepository.findAll();
    return SUCCESS;
  }

  public String openTopic() {
    this.topic = topicRepository.findOne(this.id);
    this.topic.toggleIsOpen();
    topicRepository.saveAndFlush(this.topic);
    return SUCCESS;
  }

  public String editTopic() {
    User author = userRepository.findOne(this.authorId);
    if (author != null) {
      if (this.topic.getId() == null || !topicRepository.exists(this.topic.getId())) { // create topic
        this.topic.setIsOpen(true);
        this.topic.setViews(0);
      }
      this.topic.setAuthor(author);
      topicRepository.saveAndFlush(this.topic);
      return SUCCESS;
    } else {
      return ERROR;
    }
  }

  public String deleteTopic() {
    topicRepository.delete(this.id);
    return SUCCESS;
  }

}