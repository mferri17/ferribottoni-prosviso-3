package ferribottoni.prosviso3.actions;

import java.util.List;
import com.opensymphony.xwork2.ActionSupport;
import org.springframework.beans.factory.annotation.Autowired;

import ferribottoni.prosviso3.models.User;
import ferribottoni.prosviso3.repositories.UserRepository;

public class UserAction extends ActionSupport {
  private static final long serialVersionUID = 1L;

  @Autowired
  private UserRepository userRepository;
  
  // ------- PARAMETERS

  private List<User> users;
  private User user;
  private Long id;

  public User getUser() {
    return user;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public List<User> getUsers() {
    return users;
  }

  public void setUsers(List<User> users) {
    this.users = users;
  }

  public void setUser(User user) {
    this.user = user;
  }

  // ------- ACTIONS

  public String findAll() throws Exception {
    users = userRepository.findAll();
    return SUCCESS;
  }

  public String selectUser() {
    this.user = userRepository.findOne(this.id);
    users = userRepository.findAll();
    return SUCCESS;
  }

  public String editUser() {
    userRepository.save(this.user); // if this.user.id != null the action is update, else create
    return SUCCESS;
  }

  public String deleteUser() {
    userRepository.delete(this.id);
    return SUCCESS;
  }

}