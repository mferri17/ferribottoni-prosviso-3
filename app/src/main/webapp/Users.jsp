<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>

<head>
  <meta charset="UTF-8" />
  <title>FerriBottoni - ProSviSo</title>
</head>

<body>
  <h1>Manage Users</h1>

  <p>
    <s:form action="editUser" name="editUserForm" method="post" id="editUserSubmiter">
      <s:textfield type="hidden" name="user.id" />
      <s:textfield name="user.username" label="Username*" required="required" />
      <s:textfield name="user.password" label="Password*" required="required" type="password" />
      <s:textfield name="user.name" label="Name" />
      <s:textfield name="user.surname" label="Surname" />
      <s:submit value="Submit" />
    </s:form>
  </p>

  <p>Number of rows:
    <s:property value="users.size()" />
  </p>

  <style>
    table#list td {
      padding: 5px 10px 5px 10px;
    }
  </style>

  <table id="list">
    <thead>
      <tr>
        <th>Id</th>
        <th>Username</th>
        <th>Name</th>
        <th>Surname</th>
        <th>Post Number</th>
        <th>Actions</th>
      </tr>
    </thead>

    <tbody>
      <s:iterator value="users" status="itStatus" var="u">

        <!-- row color -->
        <s:if test="#itStatus.even == true">
          <s:set var="rowColor">white</s:set>
        </s:if>
        <s:else>
          <s:set var="rowColor">#ededed</s:set>
        </s:else>

        <tr style="background-color: <s:property value='#rowColor'/>">

          <!-- row data -->
          <td>${u.id}</td>
          <td>${u.username}</td>
          <td>${u.name}</td>
          <td>${u.surname}</td>
          <td>${u.getPosts().size()}</td>

          <!-- calls to action -->
          <s:url action="selectUser" var="updateUserUrl">
            <s:param name="id">${u.id}</s:param>
          </s:url>
          <s:url action="deleteUser" var="deleteUserUrl">
            <s:param name="id">${u.id}</s:param>
          </s:url>
          <td>
            <a href="${updateUserUrl}">Update</a>
            <a href="${deleteUserUrl}">Delete</a>
          </td>
        </tr>

      </s:iterator>
    </tbody>
  </table>

  <p><a href="<s:url action='index'/>">Back to home</a><br /></p>
</body>

</html>