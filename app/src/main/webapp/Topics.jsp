<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>

<head>
  <meta charset="UTF-8" />
  <title>FerriBottoni - ProSviSo</title>
</head>

<body>
  <h1>Manage Topics</h1>

  <p>
    <s:form action="editTopic" name="editTopicForm" method="post" id="editTopicSubmiter">
      <s:textfield name="topic.id" type="hidden" />
      <s:textfield name="topic.isOpen" type="hidden" />
      <s:textfield name="topic.views" type="hidden" />
      <s:textfield name="topic.title" label="Title*" required="required" />
      <s:textfield name="topic.text" label="Text*" required="required" />
      <s:select label="Author*" headerKey="" headerValue="select author's username" list="usersSelect" listKey="id"
        listValue="username" name="authorId" value="topic.author.getId()" required="required" />
      <s:submit value="Submit" />
    </s:form>
  </p>

  <br>

  <p>Number of rows:
    <s:property value="topics.size()" />
  </p>

  <style>
    table#list td {
      padding: 5px 10px 5px 10px;
    }
  </style>

  <table id="list">
    <thead>
      <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Author</th>
        <th>Text</th>
        <th>Status</th>
        <th>Views</th>
        <th>UpdateOn</th>
        <th>Actions</th>
      </tr>
    </thead>

    <tbody>
      <s:iterator value="topics" status="topicsStatus" var="t">

        <!-- row color -->
        <s:if test="#topicsStatus.even == true">
          <s:set var="rowColor">white</s:set>
        </s:if>
        <s:else>
          <s:set var="rowColor">#ededed</s:set>
        </s:else>

        <tr style="background-color: <s:property value='#rowColor'/>">

          <!-- row data -->
          <td>${t.id}</td>
          <td>${t.title}</td>
          <td>${t.author.getUsername()}</td>
          <td>${t.text}</td>
          <td>
            <s:if test="#t.isOpen == true"> Open </s:if>
            <s:else> Closed </s:else>
            <s:url action="openTopic" var="openTopicUrl">
              <s:param name="id">${t.id}</s:param>
            </s:url>
            <a href="${openTopicUrl}" style="float:right; margin-left:5px;">(change)</a>
          </td>
          <th>${t.views}</th>
          <td>${t.updatedOn}</td>

          <!-- calls to action -->
          <s:url action="selectTopic" var="updateTopicUrl">
            <s:param name="id">${t.id}</s:param>
          </s:url>
          <s:url action="deleteTopic" var="deleteTopicUrl">
            <s:param name="id">${t.id}</s:param>
          </s:url>
          <td>
            <div style="float:right;">
              <s:if test="#t.isOpen == true"> <a href="${updateTopicUrl}">Update</a> </s:if>
              <s:else> <span title="Not available for closed topics" style="text-decoration: line-through;">Update</span> </s:else>
              <a href="${deleteTopicUrl}">Delete</a>
            </div>
          </td>
        </tr>

      </s:iterator>
    </tbody>
  </table>
  <p><a href="<s:url action='index'/>">Back to home</a><br /></p>
</body>

</html>