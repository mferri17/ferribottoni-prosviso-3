<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>

<head>
  <meta charset="UTF-8" />
  <title>FerriBottoni - ProSviSo</title>
</head>

<body>
  <h1>FerriBottoni - ProSviSo Assignment 3</h1>

  <p>
    <a href="<s:url action='users'/>">Manage Users</a><br />
    <a href="<s:url action='topics'/>">Manage Topics</a><br />
  </p>

  <p>
    <a href="<s:url action='prova'/>">Try db (users list)</a>
  </p>
</body>

</html>