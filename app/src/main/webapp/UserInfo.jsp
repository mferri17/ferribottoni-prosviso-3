<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Users</title>
</head>

<body>
    <h2>Users found (<s:property value="users.size()" />)</h2>
    <p>
        <s:property value="users" />
    </p>
</body>

</html>