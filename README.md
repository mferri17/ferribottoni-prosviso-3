# [Processo e Sviluppo del Software] Assignment 3


## Introduction

This project tries to model the basic structure of a **message board** where people can create new threads or giving their answers to the existing ones. The aim of our work is to provide the functionalities of communicating with a database, in order to persist data through the use of a JPA based Framework, making these data available to a Java WebApp built with Apache Struts2.

We also developed a very simple GUI example, just to figure out how Struts manages the control flow using the Model-View-Controller architectural pattern.

### Contributors

- _806944_ - Federico Bottoni
- *807130* - Marco Ferri

### Repository

 The source code is available on GitLab at [https://gitlab.com/mferri17/ferribottoni-prosviso-3]()



## Getting started

You just need `git` and `docker` to run the application by the following commands: 

```bash
$ git clone https://gitlab.com/mferri17/ferribottoni-prosviso-3.git
$ docker volume create pgdata
$ docker-compose up --build
```

Docker will download all needed dependencies to build and execute the application, that will be exposed on port 8080 and thus available through a web browser at the address [localhost:8080]().

### Testing the application

Once you cloned the repository, you can also decide to run tests in order to check that everything works properly. For this scope we provided a different `docker-compose` file just for running Integration Tests. You can achieve this by using the following commands:

```bash
$ git clone https://gitlab.com/mferri17/ferribottoni-prosviso-3.git
$ docker volume create pgdata
$ docker-compose -f docker-compose.test.yml build
$ docker-compose -f docker-compose.test.yml run --rm app mvn verify
```

A test run is also available on GitLab repository inspecting [CI/CD Pipelines](https://gitlab.com/mferri17/ferribottoni-prosviso-3/pipelines).



## Core Architecture

We use `Docker` to manage containerization, in order to provide developers a ready-to-use environment they can use to build and run the application. The latter is organized with 2 containers:

- *'db'* contains a simple `Postgresql` image that we use to persist data managed by the application on a database; it is reachable from outside on the address [localhost:5431]() using any `Postgresql` client installed on the host machine.
- *'app'* receives the source code in order to build and run the application; it uses a `Maven` image to manage dependencies and the whole compile-package-test process. If everything is build correctly we then pass the packaged application to a `Jetty` Web Server image that runs it and expose the WebApp on [localhost:8080](), reachable via a Web browser. This container connects with the previous one to persist data on a database.



As we already said, the Java WebApp uses `Apache Struts2` framework to manage the information exchange between back-end and front-end layers using the Model-View-Controller pattern. 

Our **back-end** uses `Struts` routing to collect requests coming to specific *URLs* and pass the control to the right *Action*. Actions represent the Controller layer, that persist data on a `Postgresql` database using the `Java Persistence API` through the usage of `Hibernate` Object Relational Mapping framework in conjunction with `Spring Data`. The latter provides some facilities, as the implementation of JPA Auditing and a simple to use Repository pattern handled with Dependency Injection.

Controllers provide data (the Model) to Views, the **front-end** developed using `JavaServer Pages (JSP)`. Our intent is to just show how you can use that Model to build dynamic web pages and then return the control to the server. Since this is our only purpose, the GUI is as simple as possible, just a little demo of the message board application with limited functionalities.

The part of the application related to JPA has been tested with `JUnit` using the `maven-failsafe-plugin` for **Integration Tests** with the database in order to verify everything that concerns with entities, CRUD and search operations works properly.



## Data Model

Application domain is described by the following UML Class Diagram. Getters and setters are omitted and properties are shown as public to improve legibility.

As you can see, a `User` can *create* and *like* many `Posts`. These can be new `Topics` or `Comments` to the existing ones (users can also reply to other comments). Also, each `User` is able to `Report` any `Post` he considers inappropriate for the message board.

`Post` and `Report` inherit from `Auditable` abstract class, that means they are managed to track when they got created and updated. This inheritance will be useful later on to work with JPA Auditing.

![models](docs/images/class-diagram-1-sm.jpg)

If you decorate the entities with appropriate Hibernate annotations, the framework is able to generate the database with a Code First approach. Image that follows in the next page is the resulting schema model of the database created by Hibernate.

The model shows we decided to handle the **inheritance** from `Post` to `Topic`/`Comment` using a *Joined Table* approach that discriminates records type on the main table (`post`) using a new column named `type`, that tells the framework which table it has to join with `post` when you decide to retrieve `Topics` or `Comments`. This approach was driven by two reasons:

- The possibility of doing **polymorphic queries** that retrieve both `Topics` and `Comments` with a single statement on `Posts`. This need excluded the *Table Per Class* strategy, that would require two different queries to work simultaneously on both tables.
- The possibility to add **not-null constraints** on `Topics` and `Comments` to preserve data consistency, that would be not possible using a *Single Table* approach.

Since `Auditable` was just a JPA Auditing related inheritance, we decided to handle it as a *MappedSuperclass* that does not produce any table on the database but replicates each property as columns on child tables.

The rest of classes to database mapping is straight forward and auto-explanatory.

![models](docs/images/diagram.jpa.png)

## Java Application Structure

In this document we assume readers can understand the content of the `app\pom.xml` file that contains the Maven configuration to retrieve all project dependencies (such as Struts, Hibernate, Spring Data, JSP and JUnit) and some useful plugins to compile, test and run the application.

While all tests can be found at `app\src\test` and that folder only contains few files, this section will mainly focus on the `app\src\main` folder, that contains everything we need to get a working application.

![folder-tree-main](docs/images/folder-tree-main-sm.png)

Inside the `main` folder you can find the following folders:

- `java`, contains Java source code for both Entities management and Struts Controllers; 
- `resources`, contains some configuration files, including `struts.xml` that manages routing;
- `webapp`, contains Struts Views (`.jsp` files) and `WEB-INF\applicationContext.xml` that defines Hibernate and Spring configurations used for both run and test the application, in addition to some directives to create or update database on startup.

### Entities and Repositories

Focusing on the folder `app\src\main\java\ferribottoni\prosviso3`, you could notice that the class diagram shown in the previous section describes the entities that can be found in the `models` folder, which Hibernate will use to create the database and developers can use to persist data. In this project we use Spring Data to help us manage persistence, letting the framework automatically create useful implementation classes for the usage of a [Repository pattern](https://martinfowler.com/eaaCatalog/repository.html). You can find repositories in the respective `repositories` folder, where you can notice each of them extends `JpaRepository` interface from *springframework* package. This interface provides CRUD operations and allows developers to define, in their own repositories, signatures for search methods that will be automatically implemented by Spring as long as developers use the right naming conventions.

Furthermore, we use `applicationContext.xml` and Spring to configure Dependency Injection to properly use repositories inside classes as *@Autowired* fields. With this technique we inject repositories into both Struts Actions and tests.

Last but not least, we use the same `applicationContext.xml` file to enable JPA Auditing, that we use on `Auditable` class and so on child classes (`Post` and `Report`). This allows you not to manually set `createdOn` and `updatedOn` properties, letting Spring handle this aspect for us when we persist an entity.



Our integration tests focus on demonstrating that entities and repositories work as expected, to check if project configuration is correct and if Hibernate annotations and search methods are properly defined for CRUD and search operations on the database with particular focus on relationships, columns constraints and cascade policies. We defined a test for each entity, excluding `Post` since it is an abstract class and we preferred to test only its children.

![folder-tree-test](docs/images/folder-tree-test.png)

Each tests class name is prefixed by IT to exclude them from `maven-surefire-plugin` Unit Testing.

### Controllers and Views

We actually just described the Model part of MVC pattern: the entities. We miss Controller and Views.

As previously mentioned, Struts calls its Controller with the name Action, and you can find them in the `actions` folder. Each Action is a class that responds to some specific URL routes, as described in `struts.xml` file. They can manage POST and GET parameters and obviously have access to back-end functionalities, with some limitations. One of these is that Struts Actions cannot actually start transactions, so it would normally be impossible to use lazy load on Controllers or Views. Luckily, Spring comes to help us once again with its `SpringOpenEntityManagerInViewFilter` that enables lazy load in Views too. This may leads to [some performance problems](https://stackoverflow.com/questions/1538222/why-not-to-use-springs-openentitymanagerinviewfilter), that - in our opinion - can be left out for our "light load" academic application. We will return on this problem later on, in the [future developments](#futureDev) section.

Getting back to Struts MVC management, you also have to know that each route is mapped with a specific method that generally returns a string. Based on this return value, Struts selects the right `.jsp` file and process it to generate HTML code (+ resources and client-side script) which sends back to the client that requested that particular URL.



## Conclusion

Looking back on our work, we can consider ourselves satisfied with what we have achieved. The structure of the project is simple and clean and certainly works as expected. Spring Data gave us a lot of help on how to properly handle the CRUD and search operations, since our main effort has been dedicated on how to set up the whole project to make it work. Configuration was not so simple, even if we used Maven and Spring, because the project actually depends on several dependencies and the configuration files were not particularly intuitive to compile to work properly. Furthermore, Hibernate is not completely clear on relationship and cascade policies combined with transaction management. Even Struts had its little problems we had to deal with to make everything work.

### Future Developments

<a name="futureDev"></a>

The project lacks some aspect of developing a WebApp and we think these are the features we need to implement first on our application:

- Remove the `SpringOpenEntityManagerInViewFilter` that enables View-side lazy load. We should instead think about providing an intermediate layer between Controller and repositories to serve transactional operations and maybe implement some other business logics.
- Integrate Spring Security both to implement a login feature and expand the Auditing functionalities.
- Obviously, we should provide a considerably better GUI and develop more structured pages, maybe using Tag Files or Apache Tiles.